import os
from os import path as osp
import torch.nn.functional as F
import joblib
import numpy as np
import pandas as pd
from tqdm import tqdm

import torch
import dgl
from dgl.data import DGLDataset
from dgl.nn.pytorch import EdgeWeightNorm
from dgl import RandomWalkPE
import dgl.function as fn

import sys 
sys.path.append('../../')
from common.dataset import GraphNodeAnomalyDectionDataset
from common.sample import CoLASubGraphSampling, UniformNeighborSampling, SLGAD_SubGraphSampling
from common.utils import load_ACM

def safe_add_self_loop(g):
    newg = dgl.remove_self_loop(g)
    newg = dgl.add_self_loop(newg)
    return newg

class CoLADataSet(DGLDataset):
    def __init__(self, base_dataset_name='Cora', subgraphsize=4):
        super(CoLADataSet).__init__()
        self.dataset_name = base_dataset_name
        self.subgraphsize = subgraphsize
        if self.dataset_name=='ACM':
            g=load_ACM()[0]
            self.oraldataset = GraphNodeAnomalyDectionDataset(name='custom',g_data=g,y_data=g.ndata['label'])
        else:
            self.oraldataset = GraphNodeAnomalyDectionDataset(name=self.dataset_name)

        self.dataset = self.oraldataset[0]
        self.colasubgraphsampler = CoLASubGraphSampling(length=self.subgraphsize)
        self.paces = []
        self.normalize_feat()
        self.random_walk_sampling()
        self.pe = RandomWalkPE(k=8)
        self.dataset = self.pe(self.dataset)
        # degree
        self.dataset.ndata['degree'] = self.dataset.in_degrees()
        
    def normalize_feat(self):
        self.dataset.ndata['feat'] = F.normalize(self.dataset.ndata['feat'], p=1, dim=1)
        norm = EdgeWeightNorm(norm='both')
        self.dataset = safe_add_self_loop(self.dataset)
        norm_edge_weight = norm(self.dataset, edge_weight=torch.ones(self.dataset.num_edges()))
        self.dataset.edata['w'] = norm_edge_weight
        # print(norm_edge_weight)

    def random_walk_sampling(self):
        self.paces = self.colasubgraphsampler(self.dataset, list(range(self.dataset.num_nodes())))

    def graph_transform(self, g):
        
        # add distance to the anchor node.
        g.ndata['distance'] = torch.ones(g.num_nodes()) * 10000
        g.ndata['distance'][0] = 0

        for _ in range(10):
            g.update_all(fn.copy_u('distance', 'neighbor'), fn.min('neighbor', 'min_n'))
            g.ndata['distance'] = torch.min(g.ndata['distance'], g.ndata['min_n']+1)
        return g

    def __getitem__(self, i):
        pos_subgraph = self.graph_transform(dgl.node_subgraph(self.dataset, self.paces[i]))
        neg_idx = np.random.randint(self.dataset.num_nodes()) 
        while neg_idx == i:
            neg_idx = np.random.randint(self.dataset.num_nodes()) 
        neg_subgraph = self.graph_transform(dgl.node_subgraph(self.dataset, self.paces[neg_idx]))
        return pos_subgraph, neg_subgraph

    def __len__(self):
        return self.dataset.num_nodes()

    def process(self):
        pass

if __name__ == '__main__':

    dataset = CoLADataSet()
    # print(dataset[0].edges())
    print(dataset[0][0].ndata['distance'])
    
    # graph, label = dataset[0]
    # print(graph, label)
